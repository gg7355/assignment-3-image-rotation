#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image create_empty_image(void);

struct image create_image(uint32_t width, uint32_t height);

void free_img_data(struct image* img);
#endif
