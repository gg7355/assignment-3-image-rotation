#include "image.h"
#include <stdbool.h>


bool read_bmp_to_image(const char* file_name, struct image* img);
bool write_image_to_bmp(const char* file_name, struct image* img);
