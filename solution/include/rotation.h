#include "image.h"
#include <stdint.h>

struct image rotate(struct image const source, int32_t angle);
