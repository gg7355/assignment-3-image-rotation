#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image create_empty_image(void) {
    return (struct image) { 0 };
}

struct image create_image(uint32_t width, uint32_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (!data) {
        fprintf(stderr, "Failed to allocate memory for image.");
        exit(1);
    }
    return (struct image) { width, height, data};
}

void free_img_data(struct image* img) {
    free(img->data);
}
