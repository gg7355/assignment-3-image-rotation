#include "bmp.h"
#include "rotation.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
	if (argc != 4) {
		fprintf(stderr, "Wrong number of arguments. You should launch the program this way: ./image-transformer <source-image> <transformed-image> <angle>\n");
		return 1;
	}
	const char* const source_image_name = argv[1];
	const char* const transformed_image_name = argv[2];
	int32_t angle = atoi(argv[3]);

	struct image img = create_empty_image();

	if (!read_bmp_to_image(source_image_name, &img))
		return 1;

	struct image trans_img = rotate(img, angle);

	if (!write_image_to_bmp(transformed_image_name, &trans_img))
		return 1;

	free_img_data(&img);
	free_img_data(&trans_img);
	return 0;
}
