#include "bmp.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define bfType 19778
#define bfReserved 0
#define bOffBits 54
#define biSize 40
#define biPlanes 1
#define biBitCount 24
#define biCompression 0
#define biXPelsPerMeter 0
#define biYPelsPerMeter 0
#define biClrUsed 0
#define biClrImportant 0

char* messages_read[] = {
        "Successful read from bmp.\n",
        "Error while reading width from bmp header.\n",
        "Error while reading height from bmp header.\n",
        "Error while reading row of a raster from bmp.\n",
        "Error while reading from bmp file: unsuccessful set of position indicator to read width and height.\n",
        "Error while reading from bmp file: unsuccessful set of position indicator to read raster.\n",
        "Error while reading from bmp file: unsuccessful set of position indicator to skip padding bytes.\n"
};

char* messages_write[] = {
        "Successful write to bmp.\n",
        "Error while writing bmp header.\n",
        "Error while writing row of a raster to bmp.\n",
        "Error while writing padding bytes to bmp.\n"
};

struct __attribute__((packed)) bmp_header
{
    uint16_t bf_Type;
    uint32_t b_fileSize;
    uint32_t bf_Reserved;
    uint32_t b_OffBits;
    uint32_t bi_Size;
    uint32_t bi_Width;
    uint32_t bi_Height;
    uint16_t bi_Planes;
    uint16_t bi_BitCount;
    uint32_t bi_Compression;
    uint32_t bi_SizeImage;
    uint32_t bi_XPelsPerMeter;
    uint32_t bi_YPelsPerMeter;
    uint32_t bi_ClrUsed;
    uint32_t bi_ClrImportant;
};

static struct bmp_header create_bmp_header_by_img(const struct image* const img) {
    uint32_t raster_size = 0;
    uint32_t width_mod_4_bytes = img->width * sizeof(struct pixel) % 4; //padding = 4 - width_mod_4_bytes
    if (width_mod_4_bytes != 0)
        raster_size = (img->width + 4 - width_mod_4_bytes) * (img->height) * sizeof(struct pixel);
    else
        raster_size = (img->width) * (img->height) * sizeof(struct pixel);
    uint32_t bfileSize = sizeof(struct bmp_header) + raster_size;
    return (struct bmp_header) {
        bfType,
        bfileSize,
        bfReserved,
        bOffBits,
        biSize,
        img->width,
        img->height,
        biPlanes,
        biBitCount,
        biCompression,
        raster_size,
        biXPelsPerMeter,
        biYPelsPerMeter,
        biClrUsed,
        biClrImportant
    };
}

enum read_status {
    READ_OK = 0,
    READ_ERROR_WIDTH_IN_HEADER,
    READ_ERROR_HEIGHT_IN_HEADER,
    READ_ERROR_ROW_OF_RASTER,
    READ_ERROR_WIDTH_AND_HEIGHT_POSITION,
    READ_ERROR_RASTER_POSITION,
    READ_ERROR_PADDING_POSITION
};

static enum read_status from_bmp(FILE* file, struct image* img) {
    uint32_t width, height;
    if (fseek(file, 18, SEEK_SET) != 0) {
        return READ_ERROR_WIDTH_AND_HEIGHT_POSITION;
    }
    if (fread(&width, sizeof(uint32_t), 1, file) != sizeof(uint32_t)) {
        if (ferror(file))
            return READ_ERROR_WIDTH_IN_HEADER;
    }
    if (fread(&height, sizeof(uint32_t), 1, file) != sizeof(uint32_t)) {
        if (ferror(file))
            return READ_ERROR_HEIGHT_IN_HEADER;
    }
    if (fseek(file, bOffBits, SEEK_SET) != 0) {
        return READ_ERROR_RASTER_POSITION;
    }
    *img = create_image(width, height);
    if ((width * sizeof(struct pixel)) % 4 == 0)
        for (int32_t i = (int32_t)(img->height - 1); i >= 0; i--) {
            if (fread(img->data + i * width, sizeof(struct pixel) * width, 1, file)
                != sizeof(struct pixel) * width)
                if (ferror(file))
                    return READ_ERROR_ROW_OF_RASTER;
        }
    else {
        for (int32_t i = (int32_t)(img->height - 1); i >= 0; i--) {
            if (fread(img->data + i * width, sizeof(struct pixel) * width, 1, file)
                != sizeof(struct pixel) * width)
                if (ferror(file))
                    return READ_ERROR_ROW_OF_RASTER;
            if (fseek(file, (long)(4 - (width * sizeof(struct pixel)) % 4), SEEK_CUR) != 0) {
                return READ_ERROR_PADDING_POSITION;
            }
        }
    }
    return READ_OK;
}

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_ROW_OF_RASTER,
    WRITE_ERROR_PADDING
};

static enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_bmp_header_by_img(img);
    if (fwrite(&header, bOffBits, 1, out) != 1) {
        if (ferror(out))
            return WRITE_ERROR_HEADER;
    }
    char nul[4] = { 0 };
    if ((sizeof(struct pixel) * img->width) % 4 == 0)
        for (int32_t i = (int32_t)(img->height - 1); i >= 0; i--) {
            if (fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, out)
                != sizeof(struct pixel) * img->width)
                if (ferror(out))
                    return WRITE_ERROR_ROW_OF_RASTER;
        }
    else {
        for (int32_t i = (int32_t)(img->height - 1); i >= 0; i--) {
            if (fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, out)
                != sizeof(struct pixel) * img->width) {
                if (ferror(out))
                    return WRITE_ERROR_ROW_OF_RASTER;
            }
            int32_t padding = (int32_t) (4 - (sizeof(struct pixel) * img->width) % 4);
            if (fwrite(&nul, 1, padding, out) != padding) {
                if (ferror(out))
                    return WRITE_ERROR_PADDING;
            }
        }
    }
    return WRITE_OK;
}

static bool handle_read_status(enum read_status status) {
    if (status == READ_OK) {
        fprintf(stdout, "%s", messages_read[status]);
        return true;
    }
    else {
        fprintf(stderr, "%s", messages_read[status]);
        return false;
    }
}

static bool handle_write_status(enum write_status status) {
    if (status == WRITE_OK) {
        fprintf(stdout, "%s", messages_write[status]);
        return true;
    }
    else {
        fprintf(stderr, "%s", messages_write[status]);
        return false;
    }
}

static FILE* handle_open(const char* const file_name, bool is_read) {
    FILE* file = NULL;

    if (is_read) {
        file = fopen(file_name, "r");
    }
    else
        file = fopen(file_name, "w");
    if (!file) {
        if (is_read)
            fprintf(stderr, "failed open bmp file to read!\n");
        else
            fprintf(stderr, "failed open bmp file to write!\n");
        return NULL;
    }
    else
        return file;
}

static bool handle_close(FILE* file, bool is_read) {
    if (fclose(file)) {
        if (is_read)
            fprintf(stderr, "failed close bmp file after reading!\n");
        else 
            fprintf(stderr, "failed close bmp file after writing!\n");
        return false;
    }
    else {
        return true;
    }
}

bool read_bmp_to_image(const char* file_name, struct image* img) {
    FILE* file = handle_open(file_name, true);
    if (!file) {
        return false;
    }

    enum read_status status = from_bmp(file, img);
    if (!handle_read_status(status))
        return false;

    if (!handle_close(file, true)) {
        return false;
    }

    return true;
}

bool write_image_to_bmp(const char* file_name, struct image* img) {
    FILE* file = handle_open(file_name, false);
    if (!file) {
        return false;
    }

    enum write_status status = to_bmp(file, img);
    if (!handle_write_status(status))
        return false;

    if (!handle_close(file, false)) {
        return false;
    }

    return true;
}
