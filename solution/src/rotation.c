#include "rotation.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h> 

static void rotate_by_90(struct image const source, struct image* rotated_image) {
    rotated_image->width = source.height;
    rotated_image->height = source.width;
    for (size_t i = 0; i < rotated_image->height; i++) {
        for (size_t j = 0; j < rotated_image->width; j++) {
            rotated_image->data[i * rotated_image->width + j] =
                source.data[(source.height - j - 1) * source.width + i];
        }
    }
}

static void rotate_by_270(struct image const source, struct image* rotated_image) {
    rotated_image->width = source.height;
    rotated_image->height = source.width;
    for (size_t i = 0; i < rotated_image->height; i++) {
        for (size_t j = 0; j < rotated_image->width; j++) {
            rotated_image->data[i * rotated_image->width + j] =
                source.data[j * source.width + source.width - i - 1];
        }
    }
}

static void rotate_by_180(struct image const source, struct image* rotated_image) {
    for (size_t i = 0; i < rotated_image->height; i++) {
        for (size_t j = 0; j < rotated_image->width; j++) {
            rotated_image->data[i * rotated_image->width + j] =
                source.data[(rotated_image->height - i - 1) * rotated_image->width + (rotated_image->width - j - 1)];
        }
    }
}

static void rotate_by_0(struct image const source, struct image* rotated_image) {
    for (size_t i = 0; i < rotated_image->height; i++) {
        for (size_t j = 0; j < rotated_image->width; j++) {
            rotated_image->data[i * rotated_image->width + j] =
                source.data[i * rotated_image->width + j];
        }
    }
}

struct image rotate(struct image const source, int32_t angle) {
    struct image rotated_image = create_image(source.width, source.height);
    switch (angle) {
        case 0: rotate_by_0(source, &rotated_image);    break;
        case 90: 
        case -270: rotate_by_90(source, &rotated_image);    break;
        case -90: 
        case 270: rotate_by_270(source, &rotated_image);    break;
        case 180: 
        case -180: rotate_by_180(source, &rotated_image);    break;
    }
    return rotated_image;
}
